![Blackintosh icon](./Desktop/Blackintosh.png)
# Blackintosh: The personal Apple Monterey-like dotfiles by SymphonyOS project
Here is my own Plasma themes which i want to add as a default Plasma theme for my SymphonyOS Plasma LiveDVD’s.☺️
## OS requirements
Any UNIX-like FOSS OS’es which have KDE Plasma 5, just like FreeBSD and Linux
## My Git mirrors
1. [`https://git.code.sf.net/p/symphony-freebsd/blacktinosh/git_dotfiles`](https://sf.net/p/symphony-freebsd/blacktinosh/git_dotfiles) (Primary project)
2. https://notabug.org/HD_Scanius/Blacintosh (Not-a-bug mirror)
3. https://GitLab.com/hd_scania/Blackintosh (GtiLab mirror)
## Gallery
![Example of my own practice](./Desktop/snap042022.png)
